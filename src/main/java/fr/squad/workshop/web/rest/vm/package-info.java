/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.squad.workshop.web.rest.vm;
