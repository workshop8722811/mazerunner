package fr.squad.games.maze.template.webservice;

import fr.squad.games.maze.template.model.Labyrinthe;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/workshop")
@Slf4j
@Tag(name = "Labyrinthes")
public class LabyrintheWS implements ILabyrintheWS {

    @Override
    public Labyrinthe labyrinthe(Long id) throws FileNotFoundException {
        Scanner scan = new Scanner(new FileReader("src/test/resources/maze/maze_" + id));
        Labyrinthe laby = new Labyrinthe();
        laby.from(scan);

        return laby;
    }
}
