package fr.squad.games.maze.template;

import fr.squad.games.maze.template.model.Labyrinthe;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import static java.lang.System.out;

public class LabyrintheApplication {

    public static void main(String[] args) throws FileNotFoundException {
        long id = 2;
        Scanner scan = new Scanner(new FileReader("src/test/resources/maze/maze_" + id));
        Labyrinthe laby = new Labyrinthe();
        laby.from(scan);

        out.println(laby.getPlan());
    }

}
