package fr.squad.games.maze.template.webservice;

import fr.squad.games.maze.template.model.Labyrinthe;
import java.io.FileNotFoundException;
import javax.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * EP GET http://localhost:8080/workshop/labyrinthes/1
 */
public interface ILabyrintheWS {
    String LABYRINTHE_ID = "labyrintheId";
    String LABYRINTHES_URL = "labyrinthes" + "/{" + LABYRINTHE_ID + ":[0-9]+}";

    @GetMapping(LABYRINTHES_URL)
    Labyrinthe labyrinthe(@PathVariable(name = LABYRINTHE_ID) @NotNull Long id) throws FileNotFoundException;
}
