package fr.squad.games.maze.template.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Scanner;

@Data
@NoArgsConstructor
public class Labyrinthe {
    String plan;

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Labyrinthe from(Scanner scan) {
        StringBuilder str = new StringBuilder();

        while (scan.hasNextLine()) {
            char[] line = scan.nextLine().toCharArray();
            for (int i = 0; i < line.length; i++) {
                str.append('?');
            }
        }

        setPlan(str.toString());

        return this;
    }

}
