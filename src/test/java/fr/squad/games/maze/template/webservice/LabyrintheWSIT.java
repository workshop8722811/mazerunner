package fr.squad.games.maze.template.webservice;

import fr.squad.MazeRunnerApp;
import fr.squad.games.maze.template.model.Labyrinthe;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@ActiveProfiles("testdev")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = MazeRunnerApp.class, webEnvironment = RANDOM_PORT)
public class LabyrintheWSIT {

    public static final String HTTP_LOCALHOST = "http://localhost:";
    @LocalServerPort
    private int port;

    @Autowired
    RestTemplateBuilder restTemplateBuilder;
    private TestRestTemplate template;

    @PostConstruct
    public void initialize() {
        RestTemplateBuilder customRestBuilder = restTemplateBuilder
                .rootUri(HTTP_LOCALHOST + port + "/workshop");
        this.template = new TestRestTemplate(customRestBuilder);
    }

    @Test
    public void _1_maze1_lecture_du_plan() {
        ResponseEntity<Labyrinthe> claimResponse = this.template.exchange(
                "/labyrinthes/1",
                HttpMethod.GET,
                null,
                Labyrinthe.class);

        Labyrinthe laby = claimResponse.getBody();

        Assert.assertTrue(laby.getPlan().length() > 1);
        Assert.assertEquals("un plan de labyrinthe pas très utile", "????", laby.getPlan());
    }

}
