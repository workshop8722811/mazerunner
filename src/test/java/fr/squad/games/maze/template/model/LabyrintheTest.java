package fr.squad.games.maze.template.model;

import fr.squad.games.maze.template.model.Labyrinthe;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.MethodName.class)
class LabyrintheTest {

    @InjectMocks
    Labyrinthe model;

    @BeforeEach
    void setUp() {
        //rien de particulier à ce stade
    }

    @Test
    void _1_un_test_c_est_deja_ca_de_pris() throws FileNotFoundException {
        Scanner scan = new Scanner(new FileReader("src/test/resources/maze/maze_1"));
        model.from(scan);
        Assert.assertEquals("un plan de labyrinthe pas très utile", "????", model.getPlan());
    }

    @Test
    void _2_pour_pas_se_prendre_les_pieds_dans_le_tapis_on_pourrait_en_ecrire_plus() {
        Assert.assertNotNull("encore heureux, merci Mockito", model);
    }

}
